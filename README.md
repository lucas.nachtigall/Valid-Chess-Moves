# Valid Chess Moves



This project draws a chess board and based on the cell selection, calculates where a Knight piece could move in exactly 2 turns.

You can see it running at: http://18.221.140.112/chess

************
Information
************
This task was made using the PHP Framework CodeIgniter.

It's architecture is based on the Model-View-Controller architectural pattern, where the main files on this project are:

Under Application folder:
Controllers: Chess.php
Views: chess/chess.php

Under Public folder:
css: css/main.css
js: js/main.js

The calculation of the possible Knight moves are sent by AJAX to the controller, which returns a JSON object that will highlight the correct cells.


*******************
Server Requirements
*******************

HTTPD Server with PHP version 5.6 or newer is recommended.

It should work on 5.3.7 as well, but we strongly advise you NOT to run
such old versions of PHP, because of potential security and performance
issues, as well as missing features.


************
Installation
************

Move all files to your server folder.



