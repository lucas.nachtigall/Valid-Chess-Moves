# Contributing to Valid-Chess-Moves

The main contributer of this project has mostly back-end experience, so the styling and responsive actions could be improved.

As a way to keep the code cleaner and optimized, the function which calculate the available Knight moves could be optimized, using a loop with the possible Knight moves:
 {[-2,1],[-1,2],[1,2],[2,1],[2,-1],[1,-2],[-1,-2],[-2,-1]}
to fill the valid positions array.

