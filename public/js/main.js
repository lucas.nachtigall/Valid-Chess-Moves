$("#remove-selected").click(function () {
    if($("#mainChessBoard").find(".selected").length > 0){
        var selected = $("#mainChessBoard").find(".selected");
        selected.removeClass("selected");
        var position = parseInt(selected.data('number'));
        var backgroundColor = parseInt((position / 8) + position) % 2 == 0 ? '#ababab' : '#ffffff';
        selected.css("background-color", backgroundColor);

    }
});

$("#find-moves").click(function () {
    if($("#mainChessBoard").find(".selected").length > 0){
        var selected = $("#mainChessBoard").find(".selected");
        $.ajax({
        url: "chess/get_moves",
        type: "post",
        dataType: "json",
        data: {pos: selected.data('pos')},
        success: function(res){
            for(var k in res) {
                $("#mainChessBoard").find("[data-pos="+res[k]+"]").addClass("green-border");
            }                
        }
        });

    }
});

$("#reset").click(function (event) {
    if($("#mainChessBoard").find(".selected").length > 0){
        var selected = $("#mainChessBoard").find(".selected");
        selected.removeClass("selected");
        var position = parseInt(selected.data('number'));
        var backgroundColor = parseInt((position / 8) + position) % 2 == 0 ? '#ababab' : '#ffffff';
        selected.css("background-color", backgroundColor);
    }
    if($("#mainChessBoard").find(".green-border").length > 0){
        var selected = $("#mainChessBoard").find(".green-border");
        selected.removeClass("green-border");
    }
});

$("div").click(function (event) {
    if(!$(event.target).hasClass("button") && !$(event.target).hasClass("number-box") && !$(event.target).hasClass("letter") ){
        if($("#mainChessBoard").find(".selected").length <= 0){
            $(event.target).css("background-color", "#8cb8ff");
            $(event.target).addClass("selected");

        }
    }
});
