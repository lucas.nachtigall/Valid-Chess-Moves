<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'chess';
$route['chess'] = 'chess';
$route['chess/get_moves'] = 'chess/get_moves';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
