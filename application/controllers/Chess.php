<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chess extends CI_Controller {

	public function index()
	{
		$this->load->view('chess/chess');
	}

	public function get_moves()
	{
		$position = $this->input->post("pos");
		$letter = $position[0];
		$number = $position[1];
		$arrayVert = array("A","B","C","D","E","F","G","H");
		$arrayHorz = array("1","2","3","4","5","6","7","8");
		$posVert = array_search($letter,$arrayVert);
		$posHorz = array_search($number,$arrayHorz);
		$validMoves = $this->move_available($posVert,$posHorz);
		$availableMoves = array();
		foreach($validMoves as $move){
			$letter = $move[0];
			$number = $move[1];
			$posVert = array_search($letter,$arrayVert);
			$posHorz = array_search($number,$arrayHorz);	
			$availableMoves[] = $this->move_available($posVert,$posHorz);
		}
		$finalMoves = array();
		foreach($availableMoves as $moveArray){
			foreach($moveArray as $move){
				if(!in_array($move,$finalMoves)){
					$finalMoves[] = $move;
				}
			}
		}

		echo json_encode($finalMoves);
		
	}


	public function move_available($posVert,$posHorz){
		$arrayVert = array("A","B","C","D","E","F","G","H");
		$arrayHorz = array("1","2","3","4","5","6","7","8");

		$validPositions = array();
		if(isset($arrayVert[$posVert-2]) && isset($arrayHorz[$posHorz+1])){
			$validPositions[] = $arrayVert[$posVert-2].$arrayHorz[$posHorz+1];
		}
		if(isset($arrayVert[$posVert-1]) && isset($arrayHorz[$posHorz+2])){
			$validPositions[] = $arrayVert[$posVert-1].$arrayHorz[$posHorz+2];
		}
		if(isset($arrayVert[$posVert+1]) && isset($arrayHorz[$posHorz+2])){
			$validPositions[] = $arrayVert[$posVert+1].$arrayHorz[$posHorz+2];
		}
		if(isset($arrayVert[$posVert+2]) && isset($arrayHorz[$posHorz+1])){
			$validPositions[] = $arrayVert[$posVert+2].$arrayHorz[$posHorz+1];
		}
		if(isset($arrayVert[$posVert+2]) && isset($arrayHorz[$posHorz-1])){
			$validPositions[] = $arrayVert[$posVert+2].$arrayHorz[$posHorz-1];
		}
		if(isset($arrayVert[$posVert+1]) && isset($arrayHorz[$posHorz-2])){
			$validPositions[] = $arrayVert[$posVert+1].$arrayHorz[$posHorz-2];
		}
		if(isset($arrayVert[$posVert-1]) && isset($arrayHorz[$posHorz-2])){
			$validPositions[] = $arrayVert[$posVert-1].$arrayHorz[$posHorz-2];
		}
		if(isset($arrayVert[$posVert-2]) && isset($arrayHorz[$posHorz-1])){
			$validPositions[] = $arrayVert[$posVert-2].$arrayHorz[$posHorz-1];
		}
		return $validPositions;
	}
}
