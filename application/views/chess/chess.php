<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">

	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Horse Movements</title>

	<link rel="stylesheet" href="/public/css/main.css">
</head>

<body>
    <div class="board">
        <div class="borderChessBoard">
            <div class="number-box"><p class="letter"></p></div>
            <div class="number-box"><p class="letter">A</p></div>
            <div class="number-box"><p class="letter">B</p></div>
            <div class="number-box"><p class="letter">C</p></div>
            <div class="number-box"><p class="letter">D</p></div>
            <div class="number-box"><p class="letter">E</p></div>
            <div class="number-box"><p class="letter">F</p></div>
            <div class="number-box"><p class="letter">G</p></div>
            <div class="number-box"><p class="letter">H</p></div>
        </div>

        <div id="mainChessBoard">
            
            <div class="number-box"><p class="letter">8</p></div>
            <div class="gray-box"  data-number="0" data-pos="A8"></div>
            <div class="white-box"  data-number="1" data-pos="B8"></div>
            <div class="gray-box"  data-number="2" data-pos="C8"></div>
            <div class="white-box"  data-number="3" data-pos="D8"></div>
            <div class="gray-box"  data-number="4" data-pos="E8"></div>
            <div class="white-box"  data-number="5" data-pos="F8"></div>
            <div class="gray-box"  data-number="6" data-pos="G8"></div>
            <div class="white-box"  data-number="7" data-pos="H8"></div>
            <div class="number-box"><p class="letter">8</p></div>

            <div class="number-box"><p class="letter">7</p></div>
            <div class="white-box"  data-number="8" data-pos="A7"></div>
            <div class="gray-box"  data-number="9" data-pos="B7"></div>
            <div class="white-box" data-number="10" data-pos="C7"></div>
            <div class="gray-box" data-number="11" data-pos="D7"></div>
            <div class="white-box" data-number="12" data-pos="E7"></div>
            <div class="gray-box" data-number="13" data-pos="F7"></div>
            <div class="white-box" data-number="14" data-pos="G7"></div>
            <div class="gray-box" data-number="15" data-pos="H7"></div>
            <div class="number-box"><p class="letter">7</p></div>

            <div class="number-box"><p class="letter">6</p></div>
            <div class="gray-box" data-number="16" data-pos="A6"></div>
            <div class="white-box" data-number="17" data-pos="B6"></div>
            <div class="gray-box" data-number="18" data-pos="C6"></div>
            <div class="white-box" data-number="19" data-pos="D6"></div>
            <div class="gray-box" data-number="20" data-pos="E6"></div>
            <div class="white-box" data-number="21" data-pos="F6"></div>
            <div class="gray-box" data-number="22" data-pos="G6"></div>
            <div class="white-box" data-number="23" data-pos="H6"></div>
            <div class="number-box"><p class="letter">6</p></div>

            <div class="number-box"><p class="letter">5</p></div>
            <div class="white-box" data-number="24" data-pos="A5"></div>
            <div class="gray-box" data-number="25" data-pos="B5"></div>
            <div class="white-box" data-number="26" data-pos="C5"></div>
            <div class="gray-box" data-number="27" data-pos="D5"></div>
            <div class="white-box" data-number="28" data-pos="E5"></div>
            <div class="gray-box" data-number="29" data-pos="F5"></div>
            <div class="white-box" data-number="30" data-pos="G5"></div>
            <div class="gray-box" data-number="31" data-pos="H5"></div>
            <div class="number-box"><p class="letter">5</p></div>

            <div class="number-box"><p class="letter">4</p></div>
            <div class="gray-box" data-number="32" data-pos="A4"></div>
            <div class="white-box" data-number="33" data-pos="B4"></div>
            <div class="gray-box" data-number="34" data-pos="C4"></div>
            <div class="white-box" data-number="35" data-pos="D4"></div>
            <div class="gray-box" data-number="36" data-pos="E4"></div>
            <div class="white-box" data-number="37" data-pos="F4"></div>
            <div class="gray-box" data-number="38" data-pos="G4"></div>
            <div class="white-box" data-number="39" data-pos="H4"></div>
            <div class="number-box"><p class="letter">4</p></div>

            <div class="number-box"><p class="letter">3</p></div>
            <div class="white-box" data-number="40" data-pos="A3"></div>
            <div class="gray-box" data-number="41" data-pos="B3"></div>
            <div class="white-box" data-number="42" data-pos="C3"></div>
            <div class="gray-box" data-number="43" data-pos="D3"></div>
            <div class="white-box" data-number="44" data-pos="E3"></div>
            <div class="gray-box" data-number="45" data-pos="F3"></div>
            <div class="white-box" data-number="46" data-pos="G3"></div>
            <div class="gray-box" data-number="47" data-pos="H3"></div>
            <div class="number-box"><p class="letter">3</p></div>

            <div class="number-box"><p class="letter">2</p></div>
            <div class="gray-box" data-number="48" data-pos="A2"></div>
            <div class="white-box" data-number="49" data-pos="B2"></div>
            <div class="gray-box" data-number="50" data-pos="C2"></div>
            <div class="white-box" data-number="51" data-pos="D2"></div>
            <div class="gray-box" data-number="52" data-pos="E2"></div>
            <div class="white-box" data-number="53" data-pos="F2"></div>
            <div class="gray-box" data-number="54" data-pos="G2"></div>
            <div class="white-box" data-number="55" data-pos="H2"></div>
            <div class="number-box"><p class="letter">2</p></div>
            
            <div class="number-box"><p class="letter">1</p></div>
            <div class="white-box" data-number="56" data-pos="A1"></div>
            <div class="gray-box" data-number="57" data-pos="B1"></div>
            <div class="white-box" data-number="58" data-pos="C1"></div>
            <div class="gray-box" data-number="59" data-pos="D1"></div>
            <div class="white-box" data-number="60" data-pos="E1"></div>
            <div class="gray-box" data-number="61" data-pos="F1"></div>
            <div class="white-box" data-number="62" data-pos="G1"></div>
            <div class="gray-box" data-number="63" data-pos="H1"></div>    
            <div class="number-box"><p class="letter">1</p></div>
        </div>

        <div class="borderChessBoard">
            <div class="number-box"><p class="letter"></p></div>
            <div class="number-box"><p class="letter">A</p></div>
            <div class="number-box"><p class="letter">B</p></div>
            <div class="number-box"><p class="letter">C</p></div>
            <div class="number-box"><p class="letter">D</p></div>
            <div class="number-box"><p class="letter">E</p></div>
            <div class="number-box"><p class="letter">F</p></div>
            <div class="number-box"><p class="letter">G</p></div>
            <div class="number-box"><p class="letter">H</p></div>
        </div>
    </div>
    
    <div class="buttons">
        <button class="button" id="remove-selected"> Remove selected</button>
        <button class="button" id="find-moves"> Find movements</button>
        <button class="button" id="reset"> Reset</button>
    </div>


</body>

</html>

<script src="/public/vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="/public/js/main.js"></script>
